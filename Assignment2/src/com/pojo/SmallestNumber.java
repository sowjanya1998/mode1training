package com.pojo;
// program to find the smallest of three numbers
public class SmallestNumber {
	int findSmallestNumber(int num1, int num2,int num3) {
		return (num1<num2)?(num1<num3?num1:num3):(num2<num3?num2:num3);
	}
	

}
