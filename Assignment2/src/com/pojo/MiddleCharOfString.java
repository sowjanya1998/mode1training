package com.pojo;

//program to find the middle character of a string

public class MiddleCharOfString {
	String toFindMiddleCharOfString(String word) {
		int stringLength = word.length();
		int middle = stringLength / 2;
		if (stringLength % 2 != 0) {
			// System.out.println("The middle character of a string is"+
			// word.charAt(middle));
			return word.substring(middle, middle + 1);
		} else {
			// System.out.println("The middle characters of a string are:"+
			// word.charAt(middle-1)+ " "+word.charAt(middle));
			return word.substring(middle - 1, middle + 1);
		}

	}
}
