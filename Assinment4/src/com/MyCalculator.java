package com;
//Interface Example


class MyCalculator implements AdvancedArithmetic {

	@Override
	public int divisorSum(int n) {
		int sum = 0;
		System.out.println("I implemented: AdvanceArithmetic");

		for (int i = 1; i <= n; i++) {
			if (n % i == 0) {
				sum = sum + i;
			}
		}

		return sum;
	}

}
