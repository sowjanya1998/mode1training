package com;
//Abstract class Example

public class Square extends Shape {
	@Override
	public String getName() {

		return super.getName();
	}

	@Override
	public void setName(String name) {

		super.setName(name);
	}

	@Override
	public float calculateArea() {
		float area = side * side;
		return area;
	}

	private int side;

	public int getSide() {
		return side;
	}

	public void setSide(int side) {
		this.side = side;
	}

	public Square(String name, int side) {
		super(name);
		this.side = side;
	}

}
