package com;
// Show the output of the following application
public class SecondClass {
	double b = 123.45;

	public SecondClass() {
		System.out.println("in the constructor of class SecondClass:");
		System.out.println("b = " + b);
		b = 3.14159;
		System.out.println("b = " + b);
	}

	public double getSecondClass() {
		return b;
	}

	public void setSecondClass(double value) {
		this.b = value;
	}

}
