package com;

public class PaybackCard extends Card {
	public PaybackCard(String holderName, String cardNumber, String expiryDate, int pointsEarned, double totalAmount) {
		super(holderName, cardNumber, expiryDate);
		this.pointsEarned = pointsEarned;
		this.totalAmount = totalAmount;
	}

	private int pointsEarned;
	double totalAmount;

	public int getPointsEarned() {
		return pointsEarned;
	}

	public PaybackCard(String holderName, String cardNumber, String expiryDate) {
		super(holderName, cardNumber, expiryDate);
	}

	public void setPointsEarned(int pointsEarned) {
		this.pointsEarned = pointsEarned;
	}

	public double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}

}
