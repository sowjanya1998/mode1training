package com;
// Show the output of the following application
public class FirstClass {
	int a = 100;

	public FirstClass() {
		System.out.println("in the constructor of class FirstClass");
		System.out.println("a = " + a);
		a = 333;
		System.out.println("a = " + a);

	}

	public int getFirstClass() {
		return a;
	}

	public void setFirstClass(int value) {
		this.a = value;
	}

}
