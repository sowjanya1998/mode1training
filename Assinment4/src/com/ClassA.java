package com;

// The following Java applications contain errors. Point out the statement(s) that contain errors. Explain what each of the errors is, and how it can be fixed. 
public class ClassA {
	private int a = 100;

	public int getA() {
		return a;
	}

	public void setA(int a) {
		this.a = a;
	}

}
