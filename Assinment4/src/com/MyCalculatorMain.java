/**
 * @author SOWJANYA
 */
package com;

import java.util.Scanner;
//Interface Example

public class MyCalculatorMain {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		AdvancedArithmetic advancedArithmetic = new MyCalculator();
		int n = scanner.nextInt();
		System.out.println("sum of divisors: " + advancedArithmetic.divisorSum(n));
		scanner.close();

	}

}
