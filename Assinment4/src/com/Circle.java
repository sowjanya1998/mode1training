package com;
//Abstract class Example
public class Circle extends Shape {
	private int radius;
	static final float PI = 3.14f;

	public int getRadius() {
		return radius;
	}

	public void setRadius(int radius) {
		this.radius = radius;
	}

	public Circle(String name, int radius) {
		super(name);
		this.radius = radius;
	}

	@Override
	public String getName() {
		return super.getName();
	}

	@Override
	public void setName(String name) {
		this.name=name;
		
	}

	@Override
	public float calculateArea() {
		float area = PI*radius*radius;
		return area;
	}
	
	

}
