/**
 * @author SOWJANYA
 */
package com;
// The following Java applications contain errors. Point out the statement(s) that contain errors. Explain what each of the errors is, and how it can be fixed. 

public class ClassAMain {

	public static void main(String[] args) {
		ClassA objClassA = new ClassA();
		System.out.println("in main(): ");
		System.out.println("classA.a = "+objClassA.getA());
		objClassA.setA(222);
		objClassA=null;

	}

}
