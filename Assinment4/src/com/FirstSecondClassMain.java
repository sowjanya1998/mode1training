/**
 * @author SOWJANYA
 */
package com;

// Show the output of the following application
public class FirstSecondClassMain {

	public static void main(String[] args) {
		FirstClass firstClass = new FirstClass();
		SecondClass secondClass = new SecondClass();
		System.out.println("in main(): ");
		System.out.println("firstClass.a = " + firstClass.getFirstClass());
		System.out.println("secondClass.a = " + secondClass.getSecondClass());
		firstClass.setFirstClass(222);
		secondClass.setSecondClass(333.33);
		System.out.println("firstClass.a = " + firstClass.getFirstClass());
		System.out.println("secondClass.a = " + secondClass.getSecondClass());
		firstClass = null;
		secondClass = null;

	}

}
