package com.hcl;

public class ModifiedString {
	static String getString(String string) {
		if (string.substring(0, 1).equals("k") && !string.substring(1, 2).equals("b")) {
			return string.substring(0, 1) + string.substring(2, string.length());
		} else if (!string.substring(0, 1).equals("k") && string.substring(0, 1).equals("b")) {
			return string.substring(1, 2) + string.substring(2, string.length());
		} else if (string.substring(0, 1).equals("k") && string.substring(1, 2).equals("b")) {
			return string;
		} else {
			return string.substring(2, string.length());
		}
	}

}
