/**
 * @author SOWJANYA
 **/
package com.hcl;

import java.util.Scanner;
//Program to convert all the characters in a string to lowercase

public class ConvertToLowercase {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		String word = scanner.next();
		System.out.println("String in lowercase is:" + word.toLowerCase());
		scanner.close();

	}

}
