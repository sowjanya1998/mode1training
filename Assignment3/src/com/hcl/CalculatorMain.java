package com.hcl;
import java.util.Scanner;
// Calculator program Main method

public class CalculatorMain {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		Calculator calculator = new Calculator();
		int num1 = scanner.nextInt();
		int num2 = scanner.nextInt();
		int result = calculator.add(num1, num2);
		System.out.println("Addition of two numbers is:"+result);
		scanner.close();
	}

}
