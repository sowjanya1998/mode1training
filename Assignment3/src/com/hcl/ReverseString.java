package com.hcl;
//program on reversing the string
public class ReverseString {
	static String reshapeString(String string, char character) {
		StringBuffer stringBuffer = new StringBuffer(string);
		StringBuffer stringBuffer1 = new StringBuffer();
		String string1 = stringBuffer.reverse().toString();
		for (int i = 0; i < string1.length(); i++) {
			stringBuffer1.append(string1).charAt(i);
			stringBuffer1.append(character);
		}
		stringBuffer1.deleteCharAt(stringBuffer1.length() - 1);
		return stringBuffer1.toString();
	}

}
