/**
 * @author SOWJANYA
 */
package com.hcl;

//import java.util.Scanner

//Program to check given String is Pangram or not
public class CheckPangram {
	public static void main(String... args) {
		// Scanner scanner = new Scanner(System.in);
		// String string = scanner.next();
		String string = "The quick brown fox jumps over the lazy dog";
		boolean[] alphaList = new boolean[26];
		int index = 0;
		int flag = 1;
		for (int i = 0; i < string.length(); i++) {
			if (string.charAt(i) >= 'A' && string.charAt(i) <= 'Z') {
				index = string.charAt(i) - 'A';
			} else if (string.charAt(i) >= 'a' && string.charAt(i) <= 'z') {
				index = string.charAt(i) - 'a';
			}
			alphaList[index] = true;
		}
		for (int i = 0; i <= 25; i++) {
			if (alphaList[i] == false) {
				flag = 0;
			}
		}
		System.out.println("String: " + string);
		if (flag == 1) {
			System.out.println("The above string is a pangram");
		} else {
			System.out.println("The above string is not a pangram");

		}
		// scanner.close();
	}

}
