/**
 * @author SOWJANYA
 */
package com.hcl;


import java.util.Scanner;
//Replace a character with another character in the string

public class ReplaceCharMain {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		String word = scanner.next();
		char oldChar = scanner.next().charAt(0);
		char newChar = scanner.next().charAt(0);
		ReplaceChar replaceChar = new ReplaceChar();
		System.out.println("After replacement the string is: "+replaceChar.replace(word, oldChar, newChar));
		scanner.close();

	}

}
