package com.hcl;

import java.util.Scanner;

public class ModifiedStringMain {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		String string = scanner.next();
		String result = ModifiedString.getString(string);
		System.out.println("The modified string is: " + result);
		scanner.close();
	}

}
