/**
 * @author SOWJANYA
 */
package com.hcl;

import java.util.Scanner;

// write a program to search an element in an array
public class SearchElement {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int n;
		System.out.println("Enter the number of elements in an array: ");
		n= scanner.nextInt();
		int[] array = new int[n];
		System.out.println("Enter the elements in an arra:");
		for(int i=0;i<array.length;i++) {
			array[i] = scanner.nextInt();
			}
		System.out.println("Enter the search element:");
		int element = scanner.nextInt();
		int flag=0;
		for(int i=0;i<array.length;i++) {
			if(array[i] == element) {
				flag=1;
				break;
			}
			else {
				flag=0;
			}
		}
		if(flag==1) {
			System.out.println("Entered element "+element+" is in the array");
		}
		else {
			System.out.println("Entered element "+element+" is not in the array");
		}
		scanner.close();
		

	}

}
