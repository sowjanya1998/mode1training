package com.hcl;
import java.util.Scanner;

public class Palindrome {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		String string= scanner.next();
		StringBuffer stringBuffer = new StringBuffer(string);
		String data = stringBuffer.toString();		
		if(string.equals(data)) {
			System.out.println("The given "+string+" string is palindrome");
			
		}
		else {
			System.out.println("The given string "+string+" is palindrome");
			
		}
		scanner.close();

	}

}
