/**
 * @author SOWJANYA
 **/
package com.hcl;

import java.util.Scanner;
//Program to sort an array of 10 elements
import java.util.Arrays;

public class SortArrayExample {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int[] array = new int[10];
		for (int i = 0; i < array.length; i++) {
			array[i] = scanner.nextInt();
		}
		Arrays.sort(array);
		System.out.println("After sorting the integer array is:");
		for (int i = 0; i < array.length; i++) {
			System.out.println(array[i]);

		}
		scanner.close();

	}

}
