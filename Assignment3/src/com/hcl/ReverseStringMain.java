/**
 * @author SOWJANYA
 */
package com.hcl;

import java.util.Scanner;
// program on reversing the string

public class ReverseStringMain {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		String string = scanner.next();
		char character = scanner.next().charAt(0);
		String result = ReverseString.reshapeString(string, character);
		System.out.println("Reverse of the string is: " + result);
		scanner.close();
	}

}
