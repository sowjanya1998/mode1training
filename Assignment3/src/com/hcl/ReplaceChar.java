package com.hcl;
// Replace a character with anothe character in the string

public class ReplaceChar {
	String replace(String sentence,char oldChar,char newChar) {
		return sentence.replace(oldChar, newChar);
	}

}
