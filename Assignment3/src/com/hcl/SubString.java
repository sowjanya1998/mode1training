
/**
 * @author SOWJANYA
 */
package com.hcl;

import java.util.Scanner;
//SubString program

public class SubString {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		String string = scanner.next();
		int fromIndex = scanner.nextInt();
		int toIndex = scanner.nextInt();
		System.out.println("The resulting subString is:" + string.substring(fromIndex, toIndex));
		scanner.close();

	}

}
