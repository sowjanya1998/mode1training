
/**
 * @author SOWJANYA
 */
package com;

import java.util.Scanner;
//program to read a number and calculate the sum of squares of even digits

public class SquaresOfEvenDigitsMain {

	public static void main(String[] args) {

		Scanner scanner = new Scanner(System.in);
		int num = scanner.nextInt();
		SquaresOfEvenDigits squaresOfEvenDigits = new SquaresOfEvenDigits();
		int value = squaresOfEvenDigits.sumOfSquaresOfEvenDigits(num);
		System.out.println("sum of squares of even digits in a number is: " + value);
		scanner.close();
		squaresOfEvenDigits = null;

	}

}
