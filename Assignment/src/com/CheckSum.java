package com;
//Program to read a number and calculate the sum of odd digits present in the given number

public class CheckSum {
	int calChecksum(String num) {
		int i=1,sum=0;
		while (i< num.length()) {
			if(i%2!=0)
			{
				sum=sum+Integer.parseInt(num.substring(i,i+1));
			}
			i=i+2;
		}
		if(sum%2==0) {
			return -1;
		}
		else {
			return 1;
		}
	}

}
