/**
 * @author sowjanya
 */
 package com;
 
import java.util.Scanner;
// Perform Operations and print Statements

public class Operator {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int num1 = scanner.nextInt();
		int num2 = scanner.nextInt();
		System.out.println("Sum of two numbers " + (num1 + num2));
		System.out.println("Subtraction of two numbers " + (num1 - num2));
		System.out.println("Multiplication of two numbers " + num1 * num2);
		System.out.println("Divison of two numbers " + num1 / num2);
		System.out.println("Modulus Divison of two numbers " + num1 % num2);
		scanner.close();

		
	}

}
