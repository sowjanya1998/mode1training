/**
 * @author SOWJANYA
 */
package com;

import java.util.Scanner;
// Program to read a number and calculate the sum of odd digits present in the given number //main functon

public class CheckSumMain {

	public static void main(String[] args) {
		CheckSum checkSum = new CheckSum();
		Scanner scanner = new Scanner(System.in);
		String num = scanner.next();
		int result = checkSum.calChecksum(num);
		System.out.println(result);
		scanner.close();
		checkSum = null;

	}

}
