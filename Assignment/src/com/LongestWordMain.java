package com;
import java.util.Scanner;
//LonestWord in the sentence main function

public class LongestWordMain {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		String sentence = scanner.next();
		LongestWord longestWord= new LongestWord();
		String result = longestWord.getLargestWord(sentence);
		System.out.println("Longest word in the sentence is: "+result);
		scanner.close();
		longestWord=null;
	
		
	}

}
