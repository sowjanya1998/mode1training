
/**
 * @author SOWJANYA
 */
package com;

import java.util.Scanner;
// Ascii value of a character

public class AsciiValueMain {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		char character =scanner.next().charAt(0);
		AsciiValue asciiValue = new AsciiValue();
		int asciiChar = asciiValue.printAsciiValue(character);
		System.out.println("Ascii Value of " +character+ " is: " + asciiChar);
		scanner.close();
		asciiValue=null;
		

	}

}
