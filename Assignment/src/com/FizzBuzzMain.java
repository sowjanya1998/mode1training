
/**
 * @author SOWJANYA
 */
package com;
//If multiples of 3 or 5 or 3&5 print fizz or buzz or fizz buzz below 100



public class FizzBuzzMain {

	public static void main(String[] args) {
		for (int i = 1; i <= 100; i++) {
			if (i % 5 == 0) {
				if (i % 3 == 0) {
					System.out.println(i+"Fizz Buzz");
				} else {
					System.out.println(i+"Bizz");
				}
			}
			if (i % 3 == 0) {
				System.out.println(i+"Fizz");
			}
		}
	}

}
