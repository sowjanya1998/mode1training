/**
 * @author SOWJANYA
 */

package com;

import java.util.Scanner;
//Swapping of two numbers

public class Swapnumbers {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int num1 = scanner.nextInt();
		int num2 = scanner.nextInt();
		System.out.println("Before swapping the numbers are: " + num1 + " " + num2);
		int temp;
		temp = num1;
		num1 = num2;
		num2 = temp;
		System.out.println("After Swapping the numbers are: " + num1 + " " + num2);
		scanner.close();
	}

}
