/**
 * 
 *@author SOWJANYA
 *
 */
package com;
//Average of three numbers

import java.util.Scanner;

public class AverageMain {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int num1 = scanner.nextInt();
		int num2 = scanner.nextInt();
		int num3 = scanner.nextInt();
		Average average = new Average();
		float result = average.calAverage(num1, num2, num3);
		System.out.println("Average of three numbers is: "+result);
		scanner.close();
		average=null;
		
	}

}

