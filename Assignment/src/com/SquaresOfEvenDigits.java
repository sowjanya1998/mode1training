package com;
// program to read a number and calculate the sum of squares of even digits

public class SquaresOfEvenDigits {
	int sumOfSquaresOfEvenDigits(int num) {
		int rem=0,sum=0;
		while (num!=0) {
			rem=num%10;
			if(rem%2==0) {
				sum=sum+rem*rem;
			}
			num=num/10;
		}
		return sum;
	}

}
