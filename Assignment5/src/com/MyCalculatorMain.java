package com;

/**
 * @author SOWJANYA
 */
import java.util.Scanner;

//3. You are required to compute the power of a number by implementing a calculator. Create a class MyCalculator which consists of a single method long power(int, int). This method takes two integers, and , as parameters and finds . If either or is negative, then the method must throw an exception which says "". Also, if both and are zero, then the method must throw an exception which says "" 
public class MyCalculatorMain {

	public static void main(String[] args) {
		MyCalculator myCalculator = new MyCalculator();
		Scanner scanner = new Scanner(System.in);

		while (scanner.hasNextInt()) {
			int n = scanner.nextInt();
			int p = scanner.nextInt();
			try {
				System.out.println(myCalculator.power(n, p));
			} catch (Exception e) {
				System.out.println(e);
			}

		}
		scanner.close();

	}

}
