package com;
//. Handling a checked exception by opening a file�

//Write�a� code�opens a text file and writes its content to the standard output. What happens if the file doesn�t exist

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

public class Handling {
	public void fileHandling() {
		try {
			InputStreamReader inputStreamReader = new InputStreamReader(new FileInputStream("C:\\eclipse\\workspace"));
			BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
			String line;
			while ((line = bufferedReader.readLine()) != null) {
				System.out.println(line);
			}
			inputStreamReader.close();
		} catch (IOException e) {
			e.printStackTrace();

		}

	}
}
