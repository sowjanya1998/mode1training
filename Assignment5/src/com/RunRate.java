/**
 * @author SOWJANYA
 */
package com;

// Given total runs scored and  total overs faced as the input. Write a program to calculate the run rate
import java.util.Scanner;

public class RunRate {
	Scanner scanner = new Scanner(System.in);
	int runs;
	int balls;
	float runRate;

	public void input() {
		try {
			System.out.println("Enter Runs Scored: ");
			runs = scanner.nextInt();
			System.out.println("Enter Balls Delivered: ");
			balls = scanner.nextInt();
		} catch (NumberFormatException e) {
			System.out.println("Error code:" + e);

		}
	}

	public void compute() {
		runRate = runs / balls;
		System.out.println(
				"Score is" + runs + " runs in " + balls + " balls with the Run Rate of " + runRate + " runs per over");
	}

	public static void main(String[] args) {
		RunRate runRate = new RunRate();
		runRate.input();
		runRate.compute();

	}

}
