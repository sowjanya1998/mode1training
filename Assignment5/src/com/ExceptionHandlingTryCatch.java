package com;

/**
 * @author SOWJANYA
 */
import java.util.InputMismatchException;
import java.util.Scanner;
//You will be given two integers and as input, you have to compute . If and are not bit signed integers or if is zero, exception will occur and you have to report it. Read sample Input/Output to know what to report in case of exceptions. 

public class ExceptionHandlingTryCatch {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		try {
			try {
				int x = scanner.nextInt();
				int y = scanner.nextInt();
				System.out.println("" + (x / y));
			} catch (InputMismatchException e) {
				System.out.println("java.util.InputMismatchException");
			}
		} catch (Exception e) {
			System.out.println(e);
		}
		scanner.close();
	}

}
