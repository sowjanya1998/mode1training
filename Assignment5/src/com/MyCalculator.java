package com;
// You are required to compute the power of a number by implementing a calculator. Create a class MyCalculator which consists of a single method long power(int, int). This method takes two integers, and , as parameters and finds . If either or is negative, then the method must throw an exception which says "". Also, if both and are zero, then the method must throw an exception which says "" 
public class MyCalculator {
	public long power(int n, int p) throws Exception {
		if (n == 0 && p == 0) {
			throw new Exception("n and p should not be zero.");
		} else if (n < 0 || p < 0) {
			throw new Exception("n or p should not be negative.");
		} else {
			return (long) (Math.pow(n, p));
		}

	}

}
